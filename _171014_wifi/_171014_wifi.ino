#define PIN_RELAY 5
// -------------------------------------------
// Serial setup
// ------------------------------------------
# define BAUD 74880
void serial_setup() {
  Serial.begin(BAUD);
  Serial.println("Initializing");
}
// ------------------------------------------
// wifi parameters
// -------------------------------------------
# include "ESP8266WiFi.h"
# include <aREST.h>
# include <aREST_UI.h>
aREST_UI rest = aREST_UI();
const char* ssid = "Napoleon's network";
const char* password = "5072620603";
// Define port to listen
#define LISTEN_PORT 80
// Create an instance of the server
WiFiServer server(LISTEN_PORT);
String ip_address = "0.0.0.0";

// ----------------------------------------
// Setup the Wifi
// ----------------------------------------
void wifi_setup() {
  // Create UI
  rest.title("Relay Control");
  rest.button(PIN_RELAY);
  // Give name and id to device
  rest.set_id("1");
  rest.set_name("esp8266");
  // Connect to wifi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  // Start the server
  server.begin();
  Serial.println("Server started");
  // Print IP
  ip_address = WiFi.localIP().toString();
  Serial.println(ip_address);
}

// ----------------------------------------
// OLED control
// ----------------------------------------
#define SDA 2
#define SCL 14
#include "SSD1306.h"
#include "SSD1306Brzo.h"
#include "Liberation_Mono.h"
SSD1306Brzo display(0x3c, SDA, SCL);
void oled_setup() {
  display.init();
  display.setContrast(255);
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(Liberation_Mono_10); // 21 characters per line

  display.flipScreenVertically(); // optional, puts the headers at the top of the screen
  display.drawString(0, 0, "Cock-a-doodle-doo!");
  display.drawString(0, 20, "WiFi connected");
  display.display();
}

// -------------------------------------
// display roost data on the oled
// -------------------------------------
void oled_roost() {
  //  char ls[22] = {};                     // line string

  Serial.println("refreshing OLED");

  display.clear();

  // ip address on line 1
  display.drawString(0, 0, "IP address:");
  display.drawString(0, 20, ip_address);

  //  // loop counter on line 2
  //  sprintf(ls, "Count: %d", counter);
  //  display.drawString(0, 20, ls);

  display.display();
}
// -----------------------------------
// Main functions
// -----------------------------------
void setup() {
  // put your setup code here, to run once:
  // Start Serial
  serial_setup();
  // WiFi
  wifi_setup();
  // OLED
  oled_setup();
}

void loop() {
  // put your main code here, to run repeatedly:
  // OLED
  static int disp_last = 0;
  if (abs(millis() - disp_last) > 5000) {
    disp_last = millis();
    oled_roost();
  }
  // WiFi
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
  while (!client.available()) {
    delay(1);
  }
  rest.handle(client);
}
