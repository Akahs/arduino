#include <Servo.h>
Servo myServo;
int servoPin = 9;
int trigPin = 11;
int echoPin = 12;
float duration, cm, dist;
float last_dist = 4;
int degree;
float alpha = 0.2;


void setup() {
  // put your setup code here, to run once:
  Serial. begin (9600);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  myServo.attach(servoPin);
}

void loop() {
  // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  pinMode(echoPin, INPUT);
  duration = pulseIn(echoPin, HIGH);
  // convert the time into a distance
  cm = (duration/2) / 29.1;
  // control servo
  cm = constrain(cm, 4, 30);
  Serial.print(cm);
  Serial.print("cm");
  Serial.println();
  dist = alpha * cm + (1-alpha) * last_dist;
  Serial.print(dist);
  Serial.print("cm");
  Serial.println();
  degree = map(dist, 4, 30, 0, 180);
  myServo.write(degree);
  last_dist = dist;
  delay(50);
}
