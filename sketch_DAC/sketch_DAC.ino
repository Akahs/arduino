#include<SPI.h>
#define PIN_nLOAD 9
#define VOLTAGE_MAX 9.0
#define DIGITAL_MAX 4095
long loop_count=0;
void transferFloat(float v){
  int d, lob, hib, bits, bitshift, Nmax, volt, voltlow;
  float voltf;
  bits = 12;
//  bitshift = 12 - bits;
  volt = (int) DIGITAL_MAX * (v/VOLTAGE_MAX);
//  voltlow = volt >> bitshift;
//  Nmax = DIGITAL_MAX >> bitshift;
//  voltf = (float) voltlow/Nmax;
//  d = (int) (DIGITAL_MAX * voltf);
  lob = (volt & 0x00ff);
  hib = (volt & 0xff00) >> 8;
  SPI.transfer(hib);
  SPI.transfer(lob);
  digitalWrite(PIN_nLOAD, LOW);
  delayMicroseconds(1);
  digitalWrite(PIN_nLOAD, HIGH);
}

void setup() {
  // put your setup code here, to run once:
  pinMode(PIN_nLOAD, OUTPUT);
  SPI.begin();
  SPI.setClockDivider(SPI_CLOCK_DIV16);
  SPI.setBitOrder(MSBFIRST);
  SPI.setDataMode(SPI_MODE0);
  // set up serial communication
  Serial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:
  int cmd;
//  char b[BUFLEN];
  float x,y;
  if (Serial.available()){
    x=Serial.parseFloat();
    if (x>=0 && x<=VOLTAGE_MAX){
      transferFloat(x);
      Serial.println("Data sent successfully!");
    }
    else
      Serial.println("Voltage out of range!");
  }
}
