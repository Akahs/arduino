int sensePin = 0;
int ledPin = 9;
int val = 0;
int ledLevel = 0;
void setup() {
  // put your setup code here, to run once:
  analogReference(DEFAULT); //isn't necessary
  pinMode(ledPin,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  val = analogRead(sensePin);
  val = constrain(val, 100, 350);
  ledLevel = map(val, 100, 350, 255, 0);
  analogWrite(ledPin,ledLevel);
}
